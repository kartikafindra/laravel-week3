<!DOCTYPE html>
<html>
<body>

    <h2> Buat Account Baru! </h2>
    <h3> Sign Up Form</h3>
    <form action="/welcome" method="post">
    @csrf
        <label> First Name:</label><br><br>
        <input type="text" name="firstname"> <br><br>
        <label> Last Name:</label><br><br>
        <input type="text" name="lastname"> <br><br>
        <label> Gender: </label><br><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label></br>
        
        <label for="other"> Nationality:</label>
        <select name="nation">
            <option value="Indo">Indonesian</option>
            <option value="Aus">Australian</option>
            <option value="Kor">Korean</option>
        </select>
        <br><br>
        <label> Languange Spoken: </label><br><br>
        <input type="checkbox" name="bahasa1">
        <label>Bahasa Indonesia</label><br>
        <input type="checkbox" name="bahasa2">
        <label>English</label><br>
        <input type="checkbox" name="bahasa3">
        <label>Other</label><br><br>
        <label> Bio: </label><br><br>
        <textarea cols="30" rows="8"></textarea><br>
       <input type="submit" value="Sign Up">
       
    </form>
