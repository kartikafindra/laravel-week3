<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    //
    protected $table ="answers";
    protected $guarded = [];

    public function pertanyaan(){
        return $this->belongsTo('App\Question', 'question_id');
    }

    public function profile(){
        return $this->belongsTo('App\Profile', 'profile_id');
    }
}
