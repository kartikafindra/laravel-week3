<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Question;
use Auth;

class PertanyaanController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except(['index', 'show']);
    }


    public function create(){
        return view('pertanyaan.create');
    }
    public function store(Request $request){
        //dd($request->all());
        $request->validate([
            'title' =>  'required|max:45',
            'content' => 'required'
        ]);
        //$query = DB::table('questions')->insert([
               // "title" => $request["title"],
                //"content" => $request["content"]
        //]);
        //eloquent
        $user = Auth :: user();
        $profile = $user->profile;
           // dd($profile->id);

        $question = new Question; 
        $question -> title= $request["title"];
        $question-> content=$request["content"];
        $question->profile_id =  $profile->id;
        $question->save(); //insert into
            
        //mass asignment
        //$question = Question::create([ //question itu dari nama model 
            //"title"=> $request["title"],
           // "content"=>$request["content"],
            //"profile_id"=> Auth::id()
       // ]);


        return redirect('/pertanyaan')->with('success', 'Your Question Has Been Saved'); 

    }

    public function index(){
        //$question = DB::table('questions')->get();
        //dd($question); 
        $question = Question ::all();
        return view('pertanyaan.index', compact('question'));
    }

    public function show($id){
       // $question = DB::table('questions')->where('id',$id)->first(); //ngambilnya satu array doang
        //dd($question);
        $question = Question::find($id);
        return view('pertanyaan.show',compact('question'));
    }

    public function edit($id){
        //$question = DB::table('questions')->where('id',$id)->first();
        $question = Question::find($id);
        return view('pertanyaan.edit',compact('question'));
    }
    public function update($id, Request $request){
        $request->validate([
            'title' =>  'required|max:45',
            'content' => 'required'
        ]);
        //$query = DB::table('questions')
                    //->where('id', $id)
                    //->update([
                       // 'title' => $request['title'],
                        //'content' => $request['content']
                   // ]);
                  
            $update = Question::where('id', $id)->update([  //eloquent
                "title" => $request["title"],
                "content"=> $request["content"]
            ]);
        return redirect('/pertanyaan')->with('success', 'Update Success');
    }
    public function destroy($id){
        //$query = DB::table('questions')
        //->where('id',$id)
        //->delete();
        Question::destroy($id); //eloquent
        return redirect('/pertanyaan')->with('success', 'Delete Success');
        
    }

}
