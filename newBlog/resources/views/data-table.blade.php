@extends('template.master')

@section('title')
Data Table Admin New

@endsection


@push('scripts')
<script src=" {{ asset('/adminLTE/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src=" {{ asset('/adminLTE/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src=" {{ asset('/adminLTE/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src=" {{ asset('/adminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush


@section('content')
@include('template.content.datatable')
@endsection


