@extends('template.master')
@section ('content')
<div class="ml-2 mt-3 mr-2">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Question {{$question->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" action="/pertanyaan/{{$question->id}}">
                  @csrf
                  @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" name="title" value="{{$question->title}}" placeholder="Enter title" required>
                    @error('title')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror 
                </div>
                  <div class="form-group">
                    <label for="content">Content Question</label>
                  <textarea class="form-control" rows="3" name="content" id="content" required>{{$question->content}} </textarea>
                  @error('content')
                        <div class="alert alert-danger">{{$message}}</div>
                   @enderror   
                </div>
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
            </div>
</div>
@endsection