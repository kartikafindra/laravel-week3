@extends('template.master')

@section('title')
Forum KasKas
@endsection
@section('content')
<div class = "mt-2">
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Question  Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @if(session('success'))
                     <div class="alert alert-success">
                    {{session('success')}}
                </div>
                  @endif
                  <a class="btn btn-primary mb-2" href="{{route('pertanyaan.create') }}"> Create New Post </a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">ID</th>
                      <th>Title</th>
                      <th>Content</th>
                      <th> Time Posted </th>
                      <th>Questioner</th>
                      <th style="width: 40px">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                      @forelse ($question as $key => $value)
                    <tr>
                        <td> {{$key + 1}} </td>
                        <td> {{$value->title}}</td>
                        <td> {{$value->content}}</td>
                        <td> {{$value->created_at}}</td>
                        <td> {{$value->penanya->full_name}}
                        <td style="display:flex;"> 
                        <a href="{{route ('pertanyaan.show', ['pertanyaan'=> $value->id])}}" class="btn btn-info btn-sm ml-2 mr-2"> Show </a>
                        <a href="{{route ('pertanyaan.edit', ['pertanyaan'=> $value->id])}}" class="btn btn-success btn-sm ml-2 mr-2"> Edit </a>
                        <form action ="{{route ('pertanyaan.destroy', ['pertanyaan'=> $value->id])}}" method="post">
                        @csrf
                        @method('Delete')
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm ml-2 mr-2">
                        </form>
                    </td>
                    </tr>
                      @empty
                      <tr>
                      <td colspan="4" align="center">No Question </td>
                      </tr>
                      @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>
            </div>
        </div>

@endsection